#Templates for multithreaded error checking#

##Installation##
Firstly clone this project
```
#!bash
git clone ...
cd error_check
```

Then clone the latest version of [DynamoRIO](https://github.com/DynamoRIO/dynamorio).
```
#!bash
mkdir dynamorio
cd dynamorio
git clone https://github.com/DynamoRIO/dynamorio.git
cd ..
mkdir dynamiorio-build
mkdir build
cd dynamiorio-build
cmake ../dynamorio
make -j
cd ../build
cmake ../src
make -j
```

##How to run the checker##
After compilation, there is a client library called **libmec.so** generated.
```
#!bash
${PATH_TO_DR_BUILD}/bin64/drrun -c libmec.so -- ${your_app_exe}
```
For example for a quick test on unix ls
```
#!bash
dynamorio-build/bin64/drrun -c libmec.so -- ls
```

##How to debug##
Rebuild dynamiorio with debug info
```
#!bash
cmake -DDEBUG=ON ../dynamorio
make -j
gdb --args dynamorio-build/bin64/drrun -debug -c libmec.so -- ls
```

