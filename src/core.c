/* DynamoRIO client for multithreaded error checking */
#include "dr_api.h"
/* DynamoRIO symbol library for retrieving function names */
#include "drsyms.h"
/* We implement our own threading library*/
#include "thread.h"
/* Libary for debugging */
#include <signal.h>

#define CHECK_THREAD_COUNT  4
#define PAGE_SIZE           4096
#define THREAD_STACK_SIZE   4096
//#define DEBUG
#define PRE_INSERT instrlist_meta_preinsert

/* Entry address of the application */
static app_pc entry_point;
/* A flag that controls faking code as application */
static volatile int return_flag = 0;
/* A flag for main thread to wait for threads creation */
static volatile int thread_started = 0;
/* A flag to hack thread id into drcontext */
static volatile int set_tls_flag = 0;

/* The stack for threads must be aligned (16)
 * Current we don't have checks for stack overflow */
uint64 thread_stack[THREAD_STACK_SIZE*CHECK_THREAD_COUNT] __attribute__((aligned(0x1000)));

/* DynamoRIO event handlers */
/* Basic block event */
static dr_emit_flags_t
event_basic_block(void *drcontext, void *tag, instrlist_t *bb,
                  bool for_trace, bool translating);
static void
thread_init(void *drcontext);

/* Call thread creation as application */
static void
create_thread_pool_handler(void *drcontext, void *tag, instrlist_t *bb);
static void
post_create_thread_pool_handler(void *drcontext, void *tag, instrlist_t *bb);
static void
dr_sym_analyse();

/* The following calls are faked as application code 
 * So that they would be translated by dynamoRIO */
static void
create_thread_pool();
static int
thread_pool(void *arg);

/* Get thread id : called from the app */
uint64 get_thread_id();
/* Called from DR */
uint64 mec_get_thread_id(uint64 stack_ptr);

/* This is called before the application starts to run */
DR_EXPORT void
dr_client_main(client_id_t id, int argc, const char *argv[])
{
    drsym_error_t r = drsym_init(0);

    /* Analyse the current executable */
    dr_sym_analyse();
    /* register your events here */
    dr_register_thread_init_event(thread_init);
    dr_register_bb_event(event_basic_block);
    /* init of return flags */
    return_flag = 0;
}

/* Read the binary header and retrieve the entry point of the program */
static void
dr_sym_analyse()
{
    module_data_t *exe_data;
    const char *exe_path;
    app_pc exe_base;
    app_pc main_addr;
    const char *appname = dr_get_application_name();
    dr_printf("App name %s\n",appname);
    /* Lookup the module data */
    exe_data = dr_lookup_module_by_name(appname);
    DR_ASSERT(exe_data != NULL);
    entry_point = (app_pc)exe_data->entry_point;
}

/* This is called immediately after the clone system call
 * if called, it means dynamoRIO take controls of the syscall */
static void
thread_init(void *drcontext)
{
#ifdef DEBUG
    dr_printf("A thread is followed by dynamoRIO!\n");
#endif
}

/* Note that this function is faked as application code by
 * create_thread_pool_handler()
 * It is called by the main thread and
 * it calls create_thread and waits for feed back from threads */
static void
create_thread_pool()
{
    int i;
    pid_t pid;
    printf("Creating threads\n");

    for (i = 0; i< CHECK_THREAD_COUNT; i++) {
        pid = create_thread(&thread_pool,
                            (void *)(long)(i+1),
                            thread_stack+THREAD_STACK_SIZE*(i+1),
                            true);
        if(pid == -1) {
            printf("thread creation error %d\n",pid);
        }
    } 
    /* Wait for feedback from threads creation
     * TODO: remove this in the future or change it
     * to atomic inc */
    while(!thread_started);
    printf("Thread creation finished\n");
    /* Memory barrier to make sure the return flag
     * is updated last */
    asm volatile ("" : : : "memory");
    return_flag = 1;
}

/* Note that this function is faked as app code,
 * Checking threads would jump here and wait in the pool
 * If the function returns, the thread will be destroyied */
static int
thread_pool(void *arg)
{
    long id = (long)arg;
    printf("Thread %ld spawned!\n",id);

    thread_started = 1;

    /* A hack to set thread id into dynamoRIO's context */
    long test_id = get_thread_id();
    printf("thread id %lx\n",test_id);
    /* Thread pool */
    /*
    while (1) {

    }
    */
}

uint64 mec_get_thread_id(uint64 stack_ptr)
{
    return (stack_ptr - (uint64)thread_stack+1) / (THREAD_STACK_SIZE<<3);
}

uint64 get_thread_id()
{
    uint64 stack_ptr;
    asm volatile ("mov %0, sp\n\t" :"=r"(stack_ptr)::);
    return mec_get_thread_id(stack_ptr);
}

/* The basic block event occurs when a new block is discovered by dynamoRIO,
 * This is the place to determine how to change the application code.
 * After returning from the call, dynamoRIO will encode it into code caches
 * and directly executes the code.
 * Later execution of the same block will directly go to code caches.
 * It would no longer trigger this event unless the code cache flushes
 * this code due to capacity */
static dr_emit_flags_t
event_basic_block(void *drcontext, void *tag, instrlist_t *bb,
                  bool for_trace, bool translating)
{
    instr_t *instr;
    /* Get the original PC from the tag */
    app_pc block_addr = dr_fragment_app_pc(tag);

    /* If the block is tagged as entry block, then create threads */
    if (block_addr == entry_point) {
        create_thread_pool_handler(drcontext,tag,bb);
    } else if (return_flag) {
        /* If the block is tagged by return flag,
         * call post create_thread */
        post_create_thread_pool_handler(drcontext,tag,bb);
    }

    /* Here is the skeleton of the remain part
       if (main_thread)
          fill_in_queue_handler();
       else
          check_queue_handler();
    */
    return DR_EMIT_DEFAULT;
}

/* An event handler that modifies the original instruction stream
 * to call create_thread_pool as application.
 * It saves all caller-saved registers, link register, frame registers
 * to the stack, inserts an indirect jump (non-meta) to create_thread_pool().
 * Since it changes the basic block, all subsequent instructions would
 * be deleted and all changes would be handled by post_create_thread_pool_hander()
 */
static void
create_thread_pool_handler(void *drcontext, void *tag, instrlist_t *bb)
{
    int i;
    instr_t *instr, *first, *second, *temp, *temp2;
#ifdef DEBUG
    //output before modifying
    instrlist_disassemble(drcontext, tag, bb, STDERR);
#endif
    /* Insert instructions between the first and second instruction 
     * it is unlikely that the second would be a target from other jumps
     * if it occurs, then it would break! */
    first = instrlist_first_app(bb);
    second = instr_get_next_app(first);
    //PRE_INSERT(bb, second, INSTR_CREATE_brk(drcontext,OPND_CREATE_INT(1)));
    /* We assume the app stack is free during the entry point,
     * so we directly spills the regsiters to the stack
     * However I found AArch64 decoder is incomplete
     * I can't encode a pre-decremet instruction! */
    /* sub sp, sp, #144 */
    instr = XINST_CREATE_sub(drcontext,
                             opnd_create_reg(DR_REG_XSP),
                             OPND_CREATE_INT16(144));               
    PRE_INSERT(bb, second, instr);
    /* Save all caller saved registers: X0-X15 */
    for (i = 0; i < 16; i += 2) {
        /* stp x(i), x(i+1), [sp, #xi_offset] */
        PRE_INSERT(bb, second, INSTR_CREATE_stp(drcontext,
                                    opnd_create_base_disp(DR_REG_XSP,
                                                          DR_REG_NULL, 0,
                                                          (i/2)*16,
                                                          OPSZ_16),
                                    opnd_create_reg(DR_REG_X0 + i),
                                    opnd_create_reg(DR_REG_X0 + i + 1)));
    }
    /* Save x29, x30 (frame and link registers) */
    instr = INSTR_CREATE_stp(drcontext,
                             opnd_create_base_disp(DR_REG_XSP,
						   DR_REG_NULL, 0,
                                                   128,
                                                   OPSZ_16),
                             opnd_create_reg(DR_REG_X29),
                             opnd_create_reg(DR_REG_X30));
    PRE_INSERT(bb, second, instr);
 
    /* Load the address of create_thread_pool to x29 */
    instrlist_insert_mov_immed_ptrsz(drcontext,
                                     (ptr_int_t)create_thread_pool,
                                     opnd_create_reg(DR_REG_X29),
                                     bb, second, &temp, &temp2);
    /* Load the return address to x30 */
    instrlist_insert_mov_immed_ptrsz(drcontext,
                                     (ptr_int_t)instr_get_app_pc(second),
                                     opnd_create_reg(DR_REG_X30),
                                     bb, second, &temp, &temp2);

    /* br create_thread_pool */
    instr = INSTR_CREATE_br(drcontext,
                            opnd_create_reg(DR_REG_X29));
    /* Note that this is not a meta instruction,
     * so we have to find a PC for the new instruction */
    instr_set_translation(instr, instr_get_app_pc(second));
    instrlist_preinsert(bb, second, instr);
    /* Since we changed the control flow of this block
     * instructions after this is never executed
     * remove all instructions afterwards to prevent dynamorio's
     * internal check */
    instr = second;
    while (instr != NULL) {
        temp = instr;
        instr = instr_get_next(instr);
        instrlist_remove(bb, temp);
        instr_destroy(drcontext, temp);
    }
#ifdef DEBUG
    //output after modifying
    instrlist_disassemble(drcontext, tag, bb, STDERR);
#endif
 
}

/* Post create thread pool handler mainly restores all the caller
 * saved regsiters from the stack */
static void
post_create_thread_pool_handler(void *drcontext, void *tag, instrlist_t *bb)
{   
    int i;
    instr_t *instr;
    instr_t *first = instrlist_first_app(bb);
    /* Restore caller saved registers */
    for (i = 0; i < 16; i += 2) {
        /* ldp x(i), x(i+1), [sp, #xi_offset] */
        PRE_INSERT(bb, first, INSTR_CREATE_ldp(drcontext,
                                    opnd_create_reg(DR_REG_X0 + i),
                                    opnd_create_reg(DR_REG_X0 + i + 1),
                                    opnd_create_base_disp(DR_REG_XSP,
                                                          DR_REG_NULL, 0,
                                                          (i/2)*16,
                                                          OPSZ_16)));
    }
    /* Restore x29, x30 */
    instr = INSTR_CREATE_ldp(drcontext,
                             opnd_create_reg(DR_REG_X29),
                             opnd_create_reg(DR_REG_X30),
                             opnd_create_base_disp(DR_REG_XSP,
						   DR_REG_NULL, 0,
                                                   128,
                                                   OPSZ_16));
    PRE_INSERT(bb, first, instr);
    /* Restore the stack pointer */
    instr = XINST_CREATE_add(drcontext,
                             opnd_create_reg(DR_REG_XSP),
                             OPND_CREATE_INT16(144));               
    PRE_INSERT(bb, first, instr);

    return_flag = 0;
#ifdef DEBUG
    //output before modifying
    instrlist_disassemble(drcontext, tag, bb, STDERR);
#endif
 
}

