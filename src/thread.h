#ifndef _MEC_THREAD_
#define _MEC_THREAD_

#define _GNU_SOURCE
#include <sched.h>
#include <linux/sched.h> 
#include "dr_api.h"

/* Multiple error checking lightweight threading library */
pid_t
create_thread(int (*fcn)(void *), void *arg, void *stack, bool share_sighand);

static void
delete_thread(pid_t pid);
#endif
