#include "thread.h"
#include <stdio.h>
void *p_tid, *c_tid;

pid_t
create_thread(int (*fcn)(void *), void *arg, void *stack, bool share_sighand)
{
    pid_t newpid;
    int flags;

    /* need SIGCHLD so parent will get that signal when child dies,
     * else have errors doing a wait */
    /* we're not doing CLONE_THREAD => child has its own pid
     * (the thread.c test tests CLONE_THREAD)
     */
    flags = (CLONE_VM | CLONE_FS | CLONE_FILES |
             (share_sighand ? CLONE_SIGHAND : 0));
    /* call linux clone */
    newpid = clone(fcn, stack, flags, arg, &p_tid, NULL, &c_tid);

    if (newpid == -1) {
        printf("smp.c: Error calling clone\n");
        return -1;
    }

    return newpid;
}

static void
delete_thread(pid_t pid)
{
    pid_t result;
    /* do not print out pids to make diff easy */
    printf("Waiting for child to exit\n");
    result = waitpid(pid, NULL, 0);
    printf("Child has exited\n");
    if (result == -1 || result != pid)
        perror("delete_thread waitpid");
}
